package in.silentsudo.springactuator.actuators;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.List;

@Endpoint(id = "silentenv")
@Component
public class AppActuators {

    @WriteOperation
    public void addUserToSudoer(@NonNull String name) {
        System.out.println("Added user: " + name + " sudoers.");
    }

    @ReadOperation
    public List<String> getSudoers() {
        return List.of("silentsudo");
    }
}
